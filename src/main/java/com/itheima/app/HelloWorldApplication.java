package com.itheima.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloWorldApplication {
    public static void main(String[] args) {
        System.out.println("success");
        SpringApplication.run(HelloWorldApplication.class, args);
    }
}
