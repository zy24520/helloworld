package com.itheima.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ghy
 * @version v1.0
 * create on 2022/11/26 11:18
 */
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        System.out.println("HelloController hello -->");
        return "Hello, SpringBoot!!";
    }

}
